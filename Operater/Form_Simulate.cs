﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operater
{
    public partial class Form_Simulate : Form
    {
        Processor SimMachine;
        int time, TimeSpan;
        int TimeMin, TimeMax;
        int[] QVal;

        public Form_Simulate()
        {
            InitializeComponent();
        }

        private void Form_Simulate_Load(object sender, EventArgs e)
        {
            TB_Sim_StrNum.Text = "8";
            TB_Sim_TimeMin.Text = "2";
            TB_Sim_TimeMax.Text = "5";
            TB_Sim_QLow.Text = "20";
            TB_Sim_QMid.Text = "12";
            TB_Sim_QHigh.Text = "4";
            TB_Sim_TimeSpan.Text = "30";

            Light_Sim.Value = 0;
            Bn_Sim_GetStat.Enabled = false;
        }

        private void Bn_Simulate_Click(object sender, EventArgs e)
        {
            if (Int32.TryParse(TB_Sim_TimeSpan.Text, out TimeSpan))
            {
                int StrNum = Int32.Parse(TB_Sim_StrNum.Text);
                TimeMin = Int32.Parse(TB_Sim_TimeMin.Text);
                TimeMax = Int32.Parse(TB_Sim_TimeMax.Text);

                QVal = null; QVal = new int[3];
                QVal[0] = 2 * Int32.Parse(TB_Sim_QLow.Text);
                QVal[1] = 2 * Int32.Parse(TB_Sim_QMid.Text);
                QVal[2] = 2 * Int32.Parse(TB_Sim_QHigh.Text);

                SimMachine = new Processor(StrNum);
                
                Light_Sim.Maximum = TimeSpan;
                time = 0;
                TimerMain.Start();

                TB_Sim_StrNum.ReadOnly = true;
                TB_Sim_TimeMin.ReadOnly = true;
                TB_Sim_TimeMax.ReadOnly = true;
                TB_Sim_QLow.ReadOnly = true;
                TB_Sim_QMid.ReadOnly = true;
                TB_Sim_QHigh.ReadOnly = true;
                TB_Sim_TimeSpan.ReadOnly = true;

                Bn_Simulate.Enabled = false;
                Bn_Sim_GetStat.Enabled = false;
            }
        }

        private void TimerMain_Tick(object sender, EventArgs e)
        {
            SimMachine.Beginer(time, QVal);
            SimMachine.Operatet_iter(time, TimeMin, TimeMax);

            Light_Sim.Value = time / 120;
            time++;

            if (time > (TimeSpan * 120))
            {
                TimerMain.Stop();

                TB_Sim_StrNum.ReadOnly = false;
                TB_Sim_TimeMin.ReadOnly = false;
                TB_Sim_TimeMax.ReadOnly = false;
                TB_Sim_QLow.ReadOnly = false;
                TB_Sim_QMid.ReadOnly = false;
                TB_Sim_QHigh.ReadOnly = false;
                TB_Sim_TimeSpan.ReadOnly = false;

                Bn_Simulate.Enabled = true;
                Bn_Sim_GetStat.Enabled = true;
            }
        }

        private void Bn_Sim_GetStat_Click(object sender, EventArgs e)
        {
            var FormStat = new Form_Status();
            this.Hide();
            FormStat.ShowDialog(this);
        }

        private void Bn_Sim_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
