﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operater
{
    public partial class Form_Entry : Form
    {
        public Form_Entry()
        {
            InitializeComponent();
        }

        private void Bn_Enter_Click(object sender, EventArgs e)
        {
            this.Hide();

            if (Sel_User.Checked)
            { 
                var FormUser = new Form_User();
                FormUser.ShowDialog(this);
            }
            if (Sel_Simulate.Checked)
            {
                var FormSim = new Form_Simulate();
                FormSim.ShowDialog(this);
            }
            if (Sel_Status.Checked)
            {
                var FormStat = new Form_Status();
                FormStat.ShowDialog(this);
            }

            this.Show();
        }

        private void Bn_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
