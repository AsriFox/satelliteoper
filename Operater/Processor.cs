﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Operater
{
    class Processor
    {
        // Названия таблиц базы данных:
        public string TableName_Users, TableName_Stream, TableName_Connect;

        public Processor(int NumOfStreams)
        {
            TableName_Users = "Satellite_UsersTable";
            TableName_Stream = "Satellite_StreamTable";
            TableName_Connect = "Satellite_ConnectTable";

            using (SqlConnection Conn = new SqlConnection(Program.ConnectionString()))
            {
                // Очистка архива соединений:
                string Request = "DELETE FROM " + TableName_Connect + "; ";

                // Создание каналов связи:
                Request += "DELETE FROM " + TableName_Stream + "; ";
                Request += "INSERT INTO " + TableName_Stream + " (Id, Proof, EndTime, Waste) VALUES ";
                for (int i = 1; i <= NumOfStreams; i++)
                {
                    if (i > 1) { Request += ","; }
                    Request += " (" + i.ToString() + ", 0, 1, 0)";
                }
                Request += "; ";

                // Сброс данных времени:
                Request += "UPDATE " + TableName_Users + " SET Request = 1, BeginTime = 0, Reject = 0;";

                SqlCommand Doit = new SqlCommand(Request, Conn);
                Doit.Connection.Open();
                Doit.ExecuteNonQuery();
                Doit.Connection.Close();
            }
        }

        public void Operatet_iter(int time, int time_min, int time_max)
        {   // Итерация обработчика очереди:
            var Adapter = new SqlDataAdapter("SELECT * FROM " + TableName_Users, Program.ConnectionString());
            var UsersTable = new DataTable();
            Adapter.Fill(UsersTable);
            Adapter.Dispose();

            Adapter = new SqlDataAdapter("SELECT * FROM " + TableName_Stream, Program.ConnectionString());
            var StreamTable = new DataTable();
            Adapter.Fill(StreamTable);
            Adapter.Dispose();

            var rnd = new Random();
            // Проход по списку абонентов:
            foreach (DataRow UserRow in UsersTable.Rows)
            {
                if (UserRow["Request"].ToString() == "1")
                {
                    // Если абонент запросил соединение, то совершается проход по списку каналов:
                    foreach (DataRow StreamRow in StreamTable.Rows)
                    {
                        if (StreamRow["Proof"].ToString() == "0")
                        {
                            // Абонент занимает первый свободный канал:
                            int buf = time + rnd.Next(120 * time_min, 120 * time_max);

                            using (SqlConnection conn = new SqlConnection(Program.ConnectionString()))
                            {
                                conn.Open();
                                SqlCommand com = conn.CreateCommand();

                                // Абонент более не запрашивает соединение:
                                com.CommandText = "UPDATE " + TableName_Users + " SET Request = 0 WHERE Id = " + UserRow["Id"].ToString() + ";";
                                com.ExecuteNonQuery();

                                // Канал теперь занят:
                                com.CommandText = "UPDATE " + TableName_Stream + " SET Proof = 1, EndTime = " + buf.ToString() + " WHERE Id = " + StreamRow["Id"].ToString() + ";";
                                com.ExecuteNonQuery();
                                StreamRow["Proof"] = 1;

                                // Создана запись в архиве соединений:
                                com.CommandText = "INSERT INTO " + TableName_Connect + " (UserId, StreamId, BeginTime, EndTime) VALUES (" + UserRow["Id"].ToString() + ", " + StreamRow["Id"].ToString() + ", " + time.ToString() + ", " + buf.ToString() + ");";
                                com.ExecuteNonQuery();

                                conn.Close();
                            }
                            break;
                        }
                    }
                }
            }
        }

        public void Beginer(int time, int[] QVal)
        {   // Итерация регулировщика соединений:
            var Adapter = new SqlDataAdapter("SELECT * FROM " + TableName_Users, Program.ConnectionString());
            var UsersTable = new DataTable();
            Adapter.Fill(UsersTable);
            Adapter.Dispose();

            Adapter = new SqlDataAdapter("SELECT * FROM " + TableName_Stream, Program.ConnectionString());
            var StreamTable = new DataTable();
            Adapter.Fill(StreamTable);
            Adapter.Dispose();

            // Проход по списку абонентов:
            foreach (DataRow UserRow in UsersTable.Rows)
            {
                if (UserRow["Request"].ToString() == "1")
                {
                    int buf_begin = Int32.Parse(UserRow["BeginTime"].ToString());
                    short buf_ql = Int16.Parse(UserRow["Quality"].ToString());

                    // Обработка отказа в обслуживании:
                    if (time > buf_begin + QVal[buf_ql])
                    {
                        int Rejects = int.Parse(UserRow["Reject"].ToString()) + 1;

                        using (SqlConnection conn = new SqlConnection(Program.ConnectionString()))
                        {
                            conn.Open();
                            SqlCommand com = conn.CreateCommand();

                            com.CommandText = "UPDATE " + TableName_Users + " SET BeginTime = " + time.ToString() + ", Reject = " + Rejects.ToString() + " WHERE Id = " + UserRow["Id"].ToString() + ";";
                            com.ExecuteNonQuery();

                            conn.Close();
                        }
                    }
                }
            }

            Random rnd = new Random();
            // Проход по списку каналов:
            foreach (DataRow StreamRow in StreamTable.Rows)
            {
                // Проверка на занятость канала:
                if (StreamRow["Proof"].ToString() == "1")
                {
                    // Проверка на окончание сеанса связи:
                    if (time >= int.Parse(StreamRow["EndTime"].ToString()))
                    {
                        using (SqlConnection conn = new SqlConnection(Program.ConnectionString()))
                        {
                            conn.Open();
                            SqlCommand com = conn.CreateCommand();

                            // Канал теперь свободен:
                            com.CommandText = "UPDATE " + TableName_Stream + " SET Proof = 0 WHERE Id = " + StreamRow["Id"].ToString() + ";";
                            com.ExecuteNonQuery();

                            com.CommandText = "SELECT UserId FROM " + TableName_Connect + " WHERE StreamId = " + StreamRow["Id"].ToString() + ";";
                            SqlDataReader reader_connect = com.ExecuteReader();

                            string connect = "";
                            // Абонент теперь запрашивает сеанс связи:
                            while (reader_connect.Read())
                            {
                                connect = reader_connect["UserId"].ToString();
                            }
                            reader_connect.Close();
                            com.CommandText = "UPDATE " + TableName_Users + " SET Request = 1 WHERE Id = " + connect + ";";
                            com.ExecuteNonQuery();

                            conn.Close();
                        }
                    }
                }
                else 
                {
                    using (SqlConnection conn = new SqlConnection(Program.ConnectionString()))
                    {
                        conn.Open();
                        SqlCommand com = conn.CreateCommand();

                        // Учёт времени простоя:
                        com.CommandText = "UPDATE " + TableName_Stream + " SET Waste = " + (int.Parse(StreamRow["Waste"].ToString()) + 1).ToString() + " WHERE Id = " + StreamRow["Id"].ToString() + ";";
                        com.ExecuteNonQuery();

                        conn.Close();
                    }
                }
            }
        }

        public static void GetStat(ref double mid_time, ref int min_time, ref int max_time, ref int waste, ref int reject)
        {
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = Program.ConnectionString();
            conn.Open();
            SqlCommand com = conn.CreateCommand();

            com.CommandText = "SELECT * From Satellite_ConnectTable";
            SqlDataReader reader = com.ExecuteReader();

            min_time = int.MaxValue;
            max_time = 0;
            mid_time = 0.0;
            int n = 0;
            while (reader.Read())
            {
                n++;
                int buf = 0;
                buf = int.Parse(reader["EndTime"].ToString()) - int.Parse(reader["BeginTime"].ToString());
                if (buf > max_time) { max_time = buf; }
                if (buf < min_time) { min_time = buf; }
                mid_time += (double)(buf);
            }
            mid_time /= n;
            reader.Close();

            com.CommandText = "SELECT Reject FROM Satellite_UsersTable";
            reader = com.ExecuteReader();
            reject = 0;
            while (reader.Read())
                { reject += int.Parse(reader["Reject"].ToString()); }
            reader.Close();

            com.CommandText = "SELECT Waste FROM Satellite_StreamTable";
            reader = com.ExecuteReader();
            waste = 0;
            while (reader.Read())
                { waste += int.Parse(reader["Waste"].ToString()); }
            reader.Close();
        }
    }
}
