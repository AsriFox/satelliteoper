﻿namespace Operater
{
    partial class Form_Status
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Bn_Stat_Gather = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TB_Stat_TimeMax = new System.Windows.Forms.TextBox();
            this.TB_Stat_TimeMin = new System.Windows.Forms.TextBox();
            this.TB_Stat_TimeAvg = new System.Windows.Forms.TextBox();
            this.TB_Stat_Waste = new System.Windows.Forms.TextBox();
            this.TB_Stat_Reject = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.Bn_Stat_Exit = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.Controls.Add(this.Bn_Stat_Gather, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.TB_Stat_TimeMax, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.TB_Stat_TimeMin, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.TB_Stat_TimeAvg, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.TB_Stat_Waste, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TB_Stat_Reject, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Bn_Stat_Exit, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(248, 153);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Bn_Stat_Gather
            // 
            this.Bn_Stat_Gather.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bn_Stat_Gather.Location = new System.Drawing.Point(4, 129);
            this.Bn_Stat_Gather.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Stat_Gather.Name = "Bn_Stat_Gather";
            this.Bn_Stat_Gather.Size = new System.Drawing.Size(144, 20);
            this.Bn_Stat_Gather.TabIndex = 10;
            this.Bn_Stat_Gather.Text = "Обновить";
            this.Bn_Stat_Gather.UseVisualStyleBackColor = true;
            this.Bn_Stat_Gather.Click += new System.EventHandler(this.Bn_Stat_Gather_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(4, 88);
            this.label4.Margin = new System.Windows.Forms.Padding(4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(144, 20);
            this.label4.TabIndex = 8;
            this.label4.Text = "Время работы (мин/макс)";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(4, 60);
            this.label3.Margin = new System.Windows.Forms.Padding(4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 20);
            this.label3.TabIndex = 7;
            this.label3.Text = "Среднее время работы";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(4, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(144, 20);
            this.label2.TabIndex = 6;
            this.label2.Text = "Полное время простоя";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // TB_Stat_TimeMax
            // 
            this.TB_Stat_TimeMax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Stat_TimeMax.Location = new System.Drawing.Point(204, 88);
            this.TB_Stat_TimeMax.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Stat_TimeMax.Name = "TB_Stat_TimeMax";
            this.TB_Stat_TimeMax.ReadOnly = true;
            this.TB_Stat_TimeMax.Size = new System.Drawing.Size(40, 20);
            this.TB_Stat_TimeMax.TabIndex = 4;
            // 
            // TB_Stat_TimeMin
            // 
            this.TB_Stat_TimeMin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Stat_TimeMin.Location = new System.Drawing.Point(156, 88);
            this.TB_Stat_TimeMin.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Stat_TimeMin.Name = "TB_Stat_TimeMin";
            this.TB_Stat_TimeMin.ReadOnly = true;
            this.TB_Stat_TimeMin.Size = new System.Drawing.Size(40, 20);
            this.TB_Stat_TimeMin.TabIndex = 3;
            // 
            // TB_Stat_TimeAvg
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.TB_Stat_TimeAvg, 2);
            this.TB_Stat_TimeAvg.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Stat_TimeAvg.Location = new System.Drawing.Point(156, 60);
            this.TB_Stat_TimeAvg.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Stat_TimeAvg.Name = "TB_Stat_TimeAvg";
            this.TB_Stat_TimeAvg.ReadOnly = true;
            this.TB_Stat_TimeAvg.Size = new System.Drawing.Size(88, 20);
            this.TB_Stat_TimeAvg.TabIndex = 2;
            // 
            // TB_Stat_Waste
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.TB_Stat_Waste, 2);
            this.TB_Stat_Waste.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Stat_Waste.Location = new System.Drawing.Point(156, 32);
            this.TB_Stat_Waste.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Stat_Waste.Name = "TB_Stat_Waste";
            this.TB_Stat_Waste.ReadOnly = true;
            this.TB_Stat_Waste.Size = new System.Drawing.Size(88, 20);
            this.TB_Stat_Waste.TabIndex = 1;
            // 
            // TB_Stat_Reject
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.TB_Stat_Reject, 2);
            this.TB_Stat_Reject.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Stat_Reject.Location = new System.Drawing.Point(156, 4);
            this.TB_Stat_Reject.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Stat_Reject.Name = "TB_Stat_Reject";
            this.TB_Stat_Reject.ReadOnly = true;
            this.TB_Stat_Reject.Size = new System.Drawing.Size(88, 20);
            this.TB_Stat_Reject.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 20);
            this.label1.TabIndex = 5;
            this.label1.Text = "Количество отказов";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Bn_Stat_Exit
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.Bn_Stat_Exit, 2);
            this.Bn_Stat_Exit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bn_Stat_Exit.Location = new System.Drawing.Point(156, 129);
            this.Bn_Stat_Exit.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Stat_Exit.Name = "Bn_Stat_Exit";
            this.Bn_Stat_Exit.Size = new System.Drawing.Size(88, 20);
            this.Bn_Stat_Exit.TabIndex = 9;
            this.Bn_Stat_Exit.Text = "Выйти";
            this.Bn_Stat_Exit.UseVisualStyleBackColor = true;
            this.Bn_Stat_Exit.Click += new System.EventHandler(this.Bn_Stat_Exit_Click);
            // 
            // Form_Status
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(248, 153);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(256, 180);
            this.Name = "Form_Status";
            this.Text = "Сбор статистики";
            this.Load += new System.EventHandler(this.Form_Status_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TB_Stat_TimeMax;
        private System.Windows.Forms.TextBox TB_Stat_TimeMin;
        private System.Windows.Forms.TextBox TB_Stat_TimeAvg;
        private System.Windows.Forms.TextBox TB_Stat_Waste;
        private System.Windows.Forms.TextBox TB_Stat_Reject;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button Bn_Stat_Gather;
        private System.Windows.Forms.Button Bn_Stat_Exit;
    }
}