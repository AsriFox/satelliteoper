﻿namespace Operater
{
    partial class Form_User
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.DataGrid_Users = new System.Windows.Forms.DataGridView();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Label_ID = new System.Windows.Forms.Label();
            this.Bn_User_Upload = new System.Windows.Forms.Button();
            this.Bn_User_Refresh = new System.Windows.Forms.Button();
            this.Bn_User_Exit = new System.Windows.Forms.Button();
            this.LB_Quality = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_Users)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel2;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Margin = new System.Windows.Forms.Padding(4);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.DataGrid_Users);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.tableLayoutPanel1);
            this.splitContainer1.Panel2MinSize = 28;
            this.splitContainer1.Size = new System.Drawing.Size(312, 293);
            this.splitContainer1.SplitterDistance = 239;
            this.splitContainer1.SplitterWidth = 1;
            this.splitContainer1.TabIndex = 0;
            // 
            // DataGrid_Users
            // 
            this.DataGrid_Users.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.DataGrid_Users.DefaultCellStyle = dataGridViewCellStyle1;
            this.DataGrid_Users.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGrid_Users.Location = new System.Drawing.Point(0, 0);
            this.DataGrid_Users.MultiSelect = false;
            this.DataGrid_Users.Name = "DataGrid_Users";
            this.DataGrid_Users.Size = new System.Drawing.Size(312, 239);
            this.DataGrid_Users.TabIndex = 1;
            this.DataGrid_Users.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGrid_Users_CellClick);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel1.Controls.Add(this.Label_ID, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Bn_User_Upload, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Bn_User_Refresh, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Bn_User_Exit, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.LB_Quality, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(312, 53);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Label_ID
            // 
            this.Label_ID.AutoSize = true;
            this.Label_ID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label_ID.Location = new System.Drawing.Point(4, 4);
            this.Label_ID.Margin = new System.Windows.Forms.Padding(4);
            this.Label_ID.Name = "Label_ID";
            this.Label_ID.Size = new System.Drawing.Size(95, 17);
            this.Label_ID.TabIndex = 6;
            this.Label_ID.Text = "ID:";
            this.Label_ID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Bn_User_Upload
            // 
            this.Bn_User_Upload.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bn_User_Upload.Location = new System.Drawing.Point(107, 29);
            this.Bn_User_Upload.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_User_Upload.Name = "Bn_User_Upload";
            this.Bn_User_Upload.Size = new System.Drawing.Size(95, 20);
            this.Bn_User_Upload.TabIndex = 3;
            this.Bn_User_Upload.Text = "Сохранить";
            this.Bn_User_Upload.UseVisualStyleBackColor = true;
            this.Bn_User_Upload.Click += new System.EventHandler(this.Bn_User_Upload_Click);
            // 
            // Bn_User_Refresh
            // 
            this.Bn_User_Refresh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bn_User_Refresh.Location = new System.Drawing.Point(4, 29);
            this.Bn_User_Refresh.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_User_Refresh.Name = "Bn_User_Refresh";
            this.Bn_User_Refresh.Size = new System.Drawing.Size(95, 20);
            this.Bn_User_Refresh.TabIndex = 1;
            this.Bn_User_Refresh.Text = "Загрузить";
            this.Bn_User_Refresh.UseVisualStyleBackColor = true;
            this.Bn_User_Refresh.Click += new System.EventHandler(this.Bn_User_Refresh_Click);
            // 
            // Bn_User_Exit
            // 
            this.Bn_User_Exit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bn_User_Exit.Location = new System.Drawing.Point(210, 29);
            this.Bn_User_Exit.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_User_Exit.Name = "Bn_User_Exit";
            this.Bn_User_Exit.Size = new System.Drawing.Size(98, 20);
            this.Bn_User_Exit.TabIndex = 2;
            this.Bn_User_Exit.Text = "Выйти";
            this.Bn_User_Exit.UseVisualStyleBackColor = true;
            this.Bn_User_Exit.Click += new System.EventHandler(this.Bn_User_Exit_Click);
            // 
            // LB_Quality
            // 
            this.LB_Quality.DropDownHeight = 50;
            this.LB_Quality.FormattingEnabled = true;
            this.LB_Quality.IntegralHeight = false;
            this.LB_Quality.Items.AddRange(new object[] {
            "Низкое",
            "Среднее",
            "Высокое"});
            this.LB_Quality.Location = new System.Drawing.Point(210, 4);
            this.LB_Quality.Margin = new System.Windows.Forms.Padding(4);
            this.LB_Quality.MaxDropDownItems = 3;
            this.LB_Quality.Name = "LB_Quality";
            this.LB_Quality.Size = new System.Drawing.Size(98, 21);
            this.LB_Quality.TabIndex = 4;
            this.LB_Quality.SelectedIndexChanged += new System.EventHandler(this.LB_Quality_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(107, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(95, 17);
            this.label1.TabIndex = 5;
            this.label1.Text = "Качество связи";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Form_User
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 293);
            this.Controls.Add(this.splitContainer1);
            this.MinimumSize = new System.Drawing.Size(240, 160);
            this.Name = "Form_User";
            this.Text = "Пользователи";
            this.Load += new System.EventHandler(this.Form_User_Load);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.DataGrid_Users)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView DataGrid_Users;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button Bn_User_Upload;
        private System.Windows.Forms.Button Bn_User_Refresh;
        private System.Windows.Forms.Button Bn_User_Exit;
        private System.Windows.Forms.Label Label_ID;
        private System.Windows.Forms.ComboBox LB_Quality;
        private System.Windows.Forms.Label label1;

    }
}