﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operater
{
    public partial class Form_Status : Form
    {
        public Form_Status()
        {
            InitializeComponent();
        }

        private void Form_Status_Load(object sender, EventArgs e)
        {
            Double AvgTime = 0.0;
            int MinTime = 0, MaxTime = 0, Waste = 0, Rejects = 0;
            Processor.GetStat(ref AvgTime, ref MinTime, ref MaxTime, ref Waste, ref Rejects);

            TB_Stat_Reject.Text = Rejects.ToString();
            TB_Stat_Waste.Text = String.Format("{0:F1}", (double)Waste / 120.0);
            TB_Stat_TimeMin.Text = String.Format("{0:F1}", (double)MinTime / 120.0);
            TB_Stat_TimeMax.Text = String.Format("{0:F1}", (double)MaxTime / 120.0);
            TB_Stat_TimeAvg.Text = String.Format("{0:F4}", AvgTime / 120.0);
        }

        private void Bn_Stat_Gather_Click(object sender, EventArgs e)
        {
            Double AvgTime = 0.0;
            int MinTime = 0, MaxTime = 0, Waste = 0, Rejects = 0;
            Processor.GetStat(ref AvgTime, ref MinTime, ref MaxTime, ref Waste, ref Rejects);

            TB_Stat_Reject.Text = Rejects.ToString();
            TB_Stat_Waste.Text = String.Format("{0:F1}", (double)Waste / 120.0);
            TB_Stat_TimeMin.Text = String.Format("{0:F1}", (double)MinTime / 120.0);
            TB_Stat_TimeMax.Text = String.Format("{0:F1}", (double)MaxTime / 120.0);
            TB_Stat_TimeAvg.Text = String.Format("{0:F4}", AvgTime / 120.0);
        }

        private void Bn_Stat_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
