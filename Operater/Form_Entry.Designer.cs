﻿namespace Operater
{
    partial class Form_Entry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Bn_Exit = new System.Windows.Forms.Button();
            this.Bn_Enter = new System.Windows.Forms.Button();
            this.Sel_User = new System.Windows.Forms.RadioButton();
            this.Sel_Simulate = new System.Windows.Forms.RadioButton();
            this.Sel_Status = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.34F));
            this.tableLayoutPanel1.Controls.Add(this.Bn_Exit, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.Bn_Enter, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.Sel_User, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Sel_Simulate, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Sel_Status, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(354, 95);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Bn_Exit
            // 
            this.Bn_Exit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bn_Exit.Location = new System.Drawing.Point(238, 71);
            this.Bn_Exit.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Exit.Name = "Bn_Exit";
            this.Bn_Exit.Size = new System.Drawing.Size(112, 20);
            this.Bn_Exit.TabIndex = 3;
            this.Bn_Exit.Text = "Выйти";
            this.Bn_Exit.UseVisualStyleBackColor = true;
            this.Bn_Exit.Click += new System.EventHandler(this.Bn_Exit_Click);
            // 
            // Bn_Enter
            // 
            this.Bn_Enter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bn_Enter.Location = new System.Drawing.Point(121, 71);
            this.Bn_Enter.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Enter.Name = "Bn_Enter";
            this.Bn_Enter.Size = new System.Drawing.Size(109, 20);
            this.Bn_Enter.TabIndex = 2;
            this.Bn_Enter.Text = "Подключиться";
            this.Bn_Enter.UseVisualStyleBackColor = true;
            this.Bn_Enter.Click += new System.EventHandler(this.Bn_Enter_Click);
            // 
            // Sel_User
            // 
            this.Sel_User.AutoSize = true;
            this.Sel_User.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Sel_User.Location = new System.Drawing.Point(4, 4);
            this.Sel_User.Margin = new System.Windows.Forms.Padding(4);
            this.Sel_User.Name = "Sel_User";
            this.Sel_User.Size = new System.Drawing.Size(109, 20);
            this.Sel_User.TabIndex = 0;
            this.Sel_User.TabStop = true;
            this.Sel_User.Text = "Пользователь";
            this.Sel_User.UseVisualStyleBackColor = true;
            // 
            // Sel_Simulate
            // 
            this.Sel_Simulate.AutoSize = true;
            this.Sel_Simulate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Sel_Simulate.Location = new System.Drawing.Point(121, 4);
            this.Sel_Simulate.Margin = new System.Windows.Forms.Padding(4);
            this.Sel_Simulate.Name = "Sel_Simulate";
            this.Sel_Simulate.Size = new System.Drawing.Size(109, 20);
            this.Sel_Simulate.TabIndex = 0;
            this.Sel_Simulate.TabStop = true;
            this.Sel_Simulate.Text = "Оператор";
            this.Sel_Simulate.UseVisualStyleBackColor = true;
            // 
            // Sel_Status
            // 
            this.Sel_Status.AutoSize = true;
            this.Sel_Status.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Sel_Status.Location = new System.Drawing.Point(238, 4);
            this.Sel_Status.Margin = new System.Windows.Forms.Padding(4);
            this.Sel_Status.Name = "Sel_Status";
            this.Sel_Status.Size = new System.Drawing.Size(112, 20);
            this.Sel_Status.TabIndex = 0;
            this.Sel_Status.TabStop = true;
            this.Sel_Status.Text = "Статистика";
            this.Sel_Status.UseVisualStyleBackColor = true;
            // 
            // Form_Entry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(354, 95);
            this.Controls.Add(this.tableLayoutPanel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "Form_Entry";
            this.Text = "Авторизация";
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button Bn_Enter;
        private System.Windows.Forms.RadioButton Sel_User;
        private System.Windows.Forms.RadioButton Sel_Simulate;
        private System.Windows.Forms.RadioButton Sel_Status;
        private System.Windows.Forms.Button Bn_Exit;
    }
}