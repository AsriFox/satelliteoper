﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operater
{
    static class Program
    {
        // public static string ConnectionString = "Data Source = 192.168.9.5; Initial Catalog = KIS; Persist Security Info = True; User ID = sa; Password = sa";
        public static string ConnectionString()
        {
            string result = "Data Source=(LocalDB)\\v11.0; AttachDbFilename=";
            result += Application.StartupPath;
            result = result.Remove(result.Length - 10, 10);
            result += "\\TestDB.mdf; Integrated Security=True";
            return result;
        }

        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form_Entry());
        }
    }
}
