﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operater
{
    public partial class Form_User : Form
    {
        public Form_User()
        {
            InitializeComponent();
        }
        private void Form_User_Load(object sender, EventArgs e)
        {
            var Request = "SELECT Satellite_UsersTable.Id, Satellite_UsersTable.Quality FROM Satellite_UsersTable";
            var Adapter = new SqlDataAdapter(Request, Program.ConnectionString());

            var AbonTable = new DataTable();
            Adapter.Fill(AbonTable);
            DataGrid_Users.DataSource = AbonTable;

            DataGrid_Users.Columns["Id"].HeaderText = "ID абонента";
            DataGrid_Users.Columns["Quality"].HeaderText = "Качество";
        }

        private void DataGrid_Users_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int SelRow = DataGrid_Users.SelectedCells[0].RowIndex;
            Label_ID.Text = "ID: " + DataGrid_Users.Rows[SelRow].Cells["Id"].Value.ToString();
            int Value;
            if (Int32.TryParse(DataGrid_Users.Rows[SelRow].Cells["Quality"].Value.ToString(), out Value))
                { LB_Quality.SelectedIndex = Value; }
        }

        private void LB_Quality_SelectedIndexChanged(object sender, EventArgs e)
        {
            int SelRow = DataGrid_Users.SelectedCells[0].RowIndex;
            int NewVal = LB_Quality.SelectedIndex;
            DataGrid_Users.Rows[SelRow].Cells["Quality"].Value = NewVal;
        }

        private void Bn_User_Refresh_Click(object sender, EventArgs e)
        {
            var Request = "SELECT Satellite_UsersTable.Id, Satellite_UsersTable.Quality FROM Satellite_UsersTable";
            var Adapter = new SqlDataAdapter(Request, Program.ConnectionString());

            var AbonTable = new DataTable();
            Adapter.Fill(AbonTable);
            DataGrid_Users.DataSource = AbonTable;

            DataGrid_Users.Columns["Id"].HeaderText = "ID абонента";
            DataGrid_Users.Columns["Quality"].HeaderText = "Качество";
        }

        private void Bn_User_Upload_Click(object sender, EventArgs e)
        {
            string FullCom = "DELETE FROM Satellite_UsersTable; ";
            FullCom += "INSERT INTO Satellite_UsersTable (Id, Quality) VALUES ";
            for (int i = 0; i < DataGrid_Users.Rows.Count - 1; i++)
            {
                if (i > 0) { FullCom += ","; }
                FullCom += " (" + DataGrid_Users.Rows[i].Cells["Id"].Value + ", " + DataGrid_Users.Rows[i].Cells["Quality"].Value + ")";
            }

            using (SqlConnection Conn = new SqlConnection(Program.ConnectionString()))
            {
                SqlCommand Doit = new SqlCommand(FullCom, Conn);
                Doit.Connection.Open();
                Doit.ExecuteNonQuery();
                Doit.Connection.Close();
            }
        }

        private void Bn_User_Exit_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
