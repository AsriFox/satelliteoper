﻿namespace Operater
{
    partial class Form_Simulate
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Bn_Sim_GetStat = new System.Windows.Forms.Button();
            this.TB_Sim_TimeSpan = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Bn_Sim_Exit = new System.Windows.Forms.Button();
            this.TB_Sim_QHigh = new System.Windows.Forms.TextBox();
            this.TB_Sim_QMid = new System.Windows.Forms.TextBox();
            this.TB_Sim_QLow = new System.Windows.Forms.TextBox();
            this.TB_Sim_TimeMax = new System.Windows.Forms.TextBox();
            this.TB_Sim_TimeMin = new System.Windows.Forms.TextBox();
            this.TB_Sim_StrNum = new System.Windows.Forms.TextBox();
            this.Bn_Simulate = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Light_Sim = new System.Windows.Forms.ProgressBar();
            this.TimerMain = new System.Windows.Forms.Timer(this.components);
            this.label4 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel1.Controls.Add(this.label4, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.Bn_Sim_GetStat, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.TB_Sim_TimeSpan, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Bn_Sim_Exit, 3, 5);
            this.tableLayoutPanel1.Controls.Add(this.TB_Sim_QHigh, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.TB_Sim_QMid, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.TB_Sim_QLow, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.TB_Sim_TimeMax, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.TB_Sim_TimeMin, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.TB_Sim_StrNum, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Bn_Simulate, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Light_Sim, 1, 5);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(312, 213);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Bn_Sim_GetStat
            // 
            this.Bn_Sim_GetStat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bn_Sim_GetStat.Location = new System.Drawing.Point(4, 189);
            this.Bn_Sim_GetStat.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Sim_GetStat.Name = "Bn_Sim_GetStat";
            this.Bn_Sim_GetStat.Size = new System.Drawing.Size(160, 20);
            this.Bn_Sim_GetStat.TabIndex = 13;
            this.Bn_Sim_GetStat.Text = "Получить статистику";
            this.Bn_Sim_GetStat.UseVisualStyleBackColor = true;
            this.Bn_Sim_GetStat.Click += new System.EventHandler(this.Bn_Sim_GetStat_Click);
            // 
            // TB_Sim_TimeSpan
            // 
            this.TB_Sim_TimeSpan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Sim_TimeSpan.Location = new System.Drawing.Point(172, 161);
            this.TB_Sim_TimeSpan.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Sim_TimeSpan.Name = "TB_Sim_TimeSpan";
            this.TB_Sim_TimeSpan.Size = new System.Drawing.Size(40, 20);
            this.TB_Sim_TimeSpan.TabIndex = 11;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(4, 60);
            this.label3.Margin = new System.Windows.Forms.Padding(4);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(160, 20);
            this.label3.TabIndex = 10;
            this.label3.Text = "Время ожидания, с";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(4, 32);
            this.label2.Margin = new System.Windows.Forms.Padding(4);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(160, 20);
            this.label2.TabIndex = 9;
            this.label2.Text = "Время работы, мин";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Bn_Sim_Exit
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.Bn_Sim_Exit, 2);
            this.Bn_Sim_Exit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bn_Sim_Exit.Location = new System.Drawing.Point(244, 189);
            this.Bn_Sim_Exit.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Sim_Exit.Name = "Bn_Sim_Exit";
            this.Bn_Sim_Exit.Size = new System.Drawing.Size(64, 20);
            this.Bn_Sim_Exit.TabIndex = 7;
            this.Bn_Sim_Exit.Text = "Выйти";
            this.Bn_Sim_Exit.UseVisualStyleBackColor = true;
            this.Bn_Sim_Exit.Click += new System.EventHandler(this.Bn_Sim_Exit_Click);
            // 
            // TB_Sim_QHigh
            // 
            this.TB_Sim_QHigh.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Sim_QHigh.Location = new System.Drawing.Point(268, 60);
            this.TB_Sim_QHigh.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Sim_QHigh.Name = "TB_Sim_QHigh";
            this.TB_Sim_QHigh.Size = new System.Drawing.Size(40, 20);
            this.TB_Sim_QHigh.TabIndex = 5;
            // 
            // TB_Sim_QMid
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.TB_Sim_QMid, 2);
            this.TB_Sim_QMid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Sim_QMid.Location = new System.Drawing.Point(220, 60);
            this.TB_Sim_QMid.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Sim_QMid.Name = "TB_Sim_QMid";
            this.TB_Sim_QMid.Size = new System.Drawing.Size(40, 20);
            this.TB_Sim_QMid.TabIndex = 4;
            // 
            // TB_Sim_QLow
            // 
            this.TB_Sim_QLow.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Sim_QLow.Location = new System.Drawing.Point(172, 60);
            this.TB_Sim_QLow.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Sim_QLow.Name = "TB_Sim_QLow";
            this.TB_Sim_QLow.Size = new System.Drawing.Size(40, 20);
            this.TB_Sim_QLow.TabIndex = 3;
            // 
            // TB_Sim_TimeMax
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.TB_Sim_TimeMax, 2);
            this.TB_Sim_TimeMax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Sim_TimeMax.Location = new System.Drawing.Point(244, 32);
            this.TB_Sim_TimeMax.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Sim_TimeMax.Name = "TB_Sim_TimeMax";
            this.TB_Sim_TimeMax.Size = new System.Drawing.Size(64, 20);
            this.TB_Sim_TimeMax.TabIndex = 2;
            // 
            // TB_Sim_TimeMin
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.TB_Sim_TimeMin, 2);
            this.TB_Sim_TimeMin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Sim_TimeMin.Location = new System.Drawing.Point(172, 32);
            this.TB_Sim_TimeMin.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Sim_TimeMin.Name = "TB_Sim_TimeMin";
            this.TB_Sim_TimeMin.Size = new System.Drawing.Size(64, 20);
            this.TB_Sim_TimeMin.TabIndex = 1;
            // 
            // TB_Sim_StrNum
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.TB_Sim_StrNum, 2);
            this.TB_Sim_StrNum.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TB_Sim_StrNum.Location = new System.Drawing.Point(172, 4);
            this.TB_Sim_StrNum.Margin = new System.Windows.Forms.Padding(4);
            this.TB_Sim_StrNum.Name = "TB_Sim_StrNum";
            this.TB_Sim_StrNum.Size = new System.Drawing.Size(64, 20);
            this.TB_Sim_StrNum.TabIndex = 0;
            // 
            // Bn_Simulate
            // 
            this.Bn_Simulate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Bn_Simulate.Location = new System.Drawing.Point(4, 161);
            this.Bn_Simulate.Margin = new System.Windows.Forms.Padding(4);
            this.Bn_Simulate.Name = "Bn_Simulate";
            this.Bn_Simulate.Size = new System.Drawing.Size(160, 20);
            this.Bn_Simulate.TabIndex = 6;
            this.Bn_Simulate.Text = "Запустить планировщик";
            this.Bn_Simulate.UseVisualStyleBackColor = true;
            this.Bn_Simulate.Click += new System.EventHandler(this.Bn_Simulate_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(4, 4);
            this.label1.Margin = new System.Windows.Forms.Padding(4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(160, 20);
            this.label1.TabIndex = 8;
            this.label1.Text = "Число каналов связи";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // Light_Sim
            // 
            this.Light_Sim.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Light_Sim.Location = new System.Drawing.Point(172, 189);
            this.Light_Sim.Margin = new System.Windows.Forms.Padding(4);
            this.Light_Sim.Maximum = 1;
            this.Light_Sim.Name = "Light_Sim";
            this.Light_Sim.Size = new System.Drawing.Size(40, 20);
            this.Light_Sim.Style = System.Windows.Forms.ProgressBarStyle.Continuous;
            this.Light_Sim.TabIndex = 12;
            // 
            // TimerMain
            // 
            this.TimerMain.Interval = 1;
            this.TimerMain.Tick += new System.EventHandler(this.TimerMain_Tick);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label4, 2);
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(220, 161);
            this.label4.Margin = new System.Windows.Forms.Padding(4);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(40, 20);
            this.label4.TabIndex = 14;
            this.label4.Text = "мин";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // Form_Simulate
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(312, 213);
            this.Controls.Add(this.tableLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(320, 180);
            this.Name = "Form_Simulate";
            this.Text = "Планировщик сеансов";
            this.Load += new System.EventHandler(this.Form_Simulate_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TextBox TB_Sim_TimeSpan;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button Bn_Sim_Exit;
        private System.Windows.Forms.TextBox TB_Sim_QHigh;
        private System.Windows.Forms.TextBox TB_Sim_QMid;
        private System.Windows.Forms.TextBox TB_Sim_QLow;
        private System.Windows.Forms.TextBox TB_Sim_TimeMax;
        private System.Windows.Forms.TextBox TB_Sim_TimeMin;
        private System.Windows.Forms.TextBox TB_Sim_StrNum;
        private System.Windows.Forms.Button Bn_Simulate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ProgressBar Light_Sim;
        private System.Windows.Forms.Button Bn_Sim_GetStat;
        private System.Windows.Forms.Timer TimerMain;
        private System.Windows.Forms.Label label4;
    }
}

